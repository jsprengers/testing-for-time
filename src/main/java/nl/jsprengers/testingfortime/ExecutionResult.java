package nl.jsprengers.testingfortime;

import java.time.LocalDateTime;

public record ExecutionResult(LocalDateTime started, LocalDateTime finished) {

}
