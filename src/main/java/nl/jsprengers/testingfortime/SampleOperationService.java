package nl.jsprengers.testingfortime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * A very simple service that simulates an operation of random length between 500 and 1500 msecs and returns the start and end times.
 */
@Service
public class SampleOperationService {

    /**
     * When not running under the test profile, Spring will inject java.time.Clock.SystemClock. No other configuration is needed
     */
    @Autowired
    Clock clock;

    @Autowired
    DateTimeService dateTimeService;

    public ExecutionResult runBatchWithCustomDateTimeService() {
        LocalDateTime started = dateTimeService.currentLocalDateTime();
        simulateLengthyOperation();
        LocalDateTime finished = dateTimeService.currentLocalDateTime();
        return new ExecutionResult(started, finished);
    }

    public ExecutionResult runBatchWithWrapper() {
        LocalDateTime started = DateTimeWrapper.currentDateTime();
        simulateLengthyOperation();
        LocalDateTime finished = DateTimeWrapper.currentDateTime();
        return new ExecutionResult(started, finished);
    }

    public ExecutionResult runBatchWithClock() {
        LocalDateTime started = LocalDateTime.now(clock);
        simulateLengthyOperation();
        LocalDateTime finished = LocalDateTime.now(clock);
        return new ExecutionResult(started, finished);
    }

    /**
     * Halts the current thread for a random period between 0,2 and 1,5 seconds
     */
    private void simulateLengthyOperation() {
        try {
            Thread.sleep(200L + (long) new Random(Instant.now().toEpochMilli()).nextInt(1300));
        } catch (InterruptedException e) {
            throw new IllegalStateException();
        }
    }

}
