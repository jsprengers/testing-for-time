package nl.jsprengers.testingfortime;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Profile("test")
@Service
public class MutableDateTimeService implements DateTimeService {

    private static LocalDateTime instant;
    private static Duration offset;

    public void setFixed(Instant instant) {
        MutableDateTimeService.instant = LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
        offset = null;
    }

    public void setOffset(Duration duration) {
        offset = duration;
        instant = null;
    }

    @Override public LocalDateTime currentLocalDateTime() {
        if (instant != null) {
            return instant;
        } else if (offset != null) {
            return LocalDateTime.now().plus(offset);
        } else {
            return LocalDateTime.now();
        }
    }
}
