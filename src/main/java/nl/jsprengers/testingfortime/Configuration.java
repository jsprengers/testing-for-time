package nl.jsprengers.testingfortime;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.time.Clock;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    @Profile("!test")
    public Clock clock(){
        return Clock.systemDefaultZone();
    }

}
