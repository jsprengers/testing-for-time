package nl.jsprengers.testingfortime;

import java.time.LocalDateTime;

public interface DateTimeService {
    LocalDateTime currentLocalDateTime();
}
