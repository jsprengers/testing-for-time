package nl.jsprengers.testingfortime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
@Import({MutableClock.class})
@ActiveProfiles("test")
public class MutableClockIntegrationTest {

    @Autowired
    SampleOperationService service;

    @Autowired
    MutableClock mutableClock;

    LocalDate fixedDate = LocalDate.of(2016, 2, 29);
    LocalTime fixedTime = LocalTime.of(12, 00, 00);
    Instant fixedInstant = LocalDateTime.of(fixedDate, fixedTime).toInstant(ZoneOffset.UTC);

    //start and finish turn out to be same. This test is just to illustrate that a fixed DateTime would be a bad choice.
    @Test
    void runCustomClockWithFixedTime() {
        mutableClock.setFixed(fixedInstant);
        var result = service.runBatchWithClock();
        assertThat(result.started()).isEqualTo(result.finished());
    }

    @Test
    void runCustomClockWithOffsetTime() {
        var now = LocalDateTime.now();// the actual now
        mutableClock.setOffset(Duration.ofHours(1));
        var result = service.runBatchWithClock();
        assertThat(result.finished()).isAfter(result.started());
        assertThat(Duration.between(result.started(), now).getSeconds()).isGreaterThan(3598);
        //do a second six hours later, i.e. 7 hours after the actual time
        mutableClock.setOffset(Duration.ofHours(7));
        var result2 = service.runBatchWithClock();
        assertThat(Duration.between(result.started(), result2.started()).getSeconds()).isGreaterThanOrEqualTo(3600 * 6);
    }

}
