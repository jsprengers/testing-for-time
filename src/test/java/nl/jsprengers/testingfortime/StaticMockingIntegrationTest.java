package nl.jsprengers.testingfortime;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
public class StaticMockingIntegrationTest {

    @Autowired
    SampleOperationService service;
    LocalDateTime fixedLocalDateTime = LocalDateTime.of(LocalDate.of(2016, 2, 29), LocalTime.of(12, 00, 00));

    @Test
    void runMockedDateTimeWithFixedTime() {
        try (MockedStatic<LocalDateTime> mockedStatic = Mockito.mockStatic(LocalDateTime.class)) {
            mockedStatic.when(() -> LocalDateTime.now(ArgumentMatchers.any(Clock.class))).thenReturn(fixedLocalDateTime);
            var result = service.runBatchWithClock();
            assertThat(result.started()).isEqualTo(result.finished());
        }
        //notice that the static mocking is only in effect within the above try block, which is of course how we would want it.
        assertThat(LocalDateTime.now().getYear()).isGreaterThanOrEqualTo(2022);
    }

}
