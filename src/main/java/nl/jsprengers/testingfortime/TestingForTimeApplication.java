package nl.jsprengers.testingfortime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.time.Clock;

@SpringBootApplication
public class TestingForTimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestingForTimeApplication.class, args);
    }

}
