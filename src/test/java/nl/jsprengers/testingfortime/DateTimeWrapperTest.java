package nl.jsprengers.testingfortime;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

public class DateTimeWrapperTest {

    LocalDate fixedDate = LocalDate.of(2016, 2, 29);
    LocalTime fixedTime = LocalTime.of(12, 00, 00);
    Instant fixedInstant = LocalDateTime.of(fixedDate, fixedTime).toInstant(ZoneOffset.UTC);

    @Test
    void testFixed() {
        DateTimeWrapper.setFixed(fixedInstant);
        var ldt = DateTimeWrapper.currentDateTime();
        assertThat(ldt.format(DateTimeFormatter.ISO_DATE_TIME)).isEqualTo("2016-02-29T12:00:00");
    }

    @Test
    void testOffset() throws InterruptedException {
        LocalDateTime now = LocalDateTime.now();
        DateTimeWrapper.setOffset(Duration.ofMinutes(2));
        var twoMinutesLater = DateTimeWrapper.currentDateTime();
        var difference = Duration.between(now, twoMinutesLater);
        assertThat(difference.get(ChronoUnit.SECONDS)).isEqualTo(120);

        Thread.sleep(1100);
        var oneSecondLater = DateTimeWrapper.currentDateTime();
        assertThat(oneSecondLater).isAfter(twoMinutesLater);
    }

}
