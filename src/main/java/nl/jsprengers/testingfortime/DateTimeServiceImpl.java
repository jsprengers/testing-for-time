package nl.jsprengers.testingfortime;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Profile("!test")
@Service
public class DateTimeServiceImpl implements DateTimeService{

    public LocalDateTime currentLocalDateTime(){
        return LocalDateTime.now();
    }
}
