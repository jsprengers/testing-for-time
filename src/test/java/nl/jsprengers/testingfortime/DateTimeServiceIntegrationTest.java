package nl.jsprengers.testingfortime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
@Import(MutableDateTimeService.class)
@ActiveProfiles("test")
public class DateTimeServiceIntegrationTest {

    @Autowired
    SampleOperationService service;

    @Autowired
    MutableDateTimeService mutableDateService;

    LocalDate fixedDate = LocalDate.of(2016, 2, 29);
    LocalTime fixedTime = LocalTime.of(12, 00, 00);
    Instant fixedInstant = LocalDateTime.of(fixedDate, fixedTime).toInstant(ZoneOffset.UTC);

    //start and finish turn out to be same. This test is just to illustrate that a fixed DateTime would be a bad choice.
    @Test
    void runCustomDateTimeServiceWithFixedTime() {
        mutableDateService.setFixed(fixedInstant);
        var result = service.runBatchWithCustomDateTimeService();

        assertThat(result.started()).isEqualTo(result.finished());
    }

    @Test
    void runCustomDateTimeServiceWithOffsetTime() {
        var now = LocalDateTime.now();// the actual now
        mutableDateService.setOffset(Duration.ofHours(1));
        var result = service.runBatchWithCustomDateTimeService();
        assertThat(result.finished()).isAfter(result.started());
        assertThat(Duration.between(now, result.started()).getSeconds()).isEqualTo(3600);
    }

}
