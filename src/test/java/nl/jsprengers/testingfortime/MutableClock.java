package nl.jsprengers.testingfortime;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

@Profile("test")
@Service
public class MutableClock extends Clock {

    private Instant instant;
    private Duration offset;

    public void setFixed(Instant instant) {
        this.instant = instant;
    }

    public void setOffset(Duration duration) {
        offset = duration;
    }

    @Override public ZoneId getZone() {
        return ZoneId.of("UTC");
    }

    @Override public Clock withZone(ZoneId zone) {
        throw new IllegalAccessError("Not supported");
    }

    @Override public Instant instant() {
        if (instant != null) {
            return instant;
        } else if (offset != null){
            return Instant.now().plus(offset);
        } else {
            return Instant.now();
        }
    }
}
