package nl.jsprengers.testingfortime;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest()
public class DateTimeWrapperIntegrationTest {

    @Autowired
    SampleOperationService service;


    LocalDate fixedDate = LocalDate.of(2016, 2, 29);
    LocalTime fixedTime = LocalTime.of(12, 00, 00);
    Instant fixedInstant = LocalDateTime.of(fixedDate, fixedTime).toInstant(ZoneOffset.UTC);

    //start and finish turn out to be same. This test is just to illustrate that a fixed DateTime would be a bad choice.
    @Test
    void runCustomDateTimeServiceWithFixedTime() {
        DateTimeWrapper.setFixed(fixedInstant);
        var result = service.runBatchWithWrapper();
        assertThat(result.started()).isEqualTo(result.finished());
    }

    @Test
    void runCustomDateTimeServiceWithOffsetTime() {
        var now = LocalDateTime.now();// the actual now
        DateTimeWrapper.setOffset(Duration.ofHours(1));
        var result = service.runBatchWithWrapper();
        assertThat(result.finished()).isAfter(result.started());
        assertThat(Duration.between(now, result.started()).getSeconds()).isEqualTo(3600);
    }

}
